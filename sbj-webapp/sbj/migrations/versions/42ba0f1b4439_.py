"""empty message

Revision ID: 42ba0f1b4439
Revises: 
Create Date: 2023-01-21 00:13:14.986214

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '42ba0f1b4439'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('cards',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('h_value', sa.Integer(), nullable=False),
    sa.Column('l_value', sa.Integer(), nullable=False),
    sa.Column('face', sa.String(length=3), nullable=True),
    sa.Column('suite', sa.String(length=8), nullable=True),
    sa.Column('url', sa.Text(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('face')
    )
    op.create_table('decks',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('games',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('game_status', sa.String(), nullable=False),
    sa.Column('started_at', sa.DateTime(), nullable=False),
    sa.Column('finished_at', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('hands',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('status', sa.String(), nullable=False),
    sa.Column('player_limit', sa.Integer(), nullable=False),
    sa.Column('h_value', sa.Integer(), nullable=False),
    sa.Column('l_value', sa.Integer(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('players',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('name', sa.String(length=128), nullable=False),
    sa.Column('limit', sa.Integer(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('deckcards',
    sa.Column('deck_id', sa.Integer(), nullable=False),
    sa.Column('card_id', sa.Integer(), nullable=False),
    sa.Column('used', sa.Boolean(), nullable=True),
    sa.ForeignKeyConstraint(['card_id'], ['cards.id'], ),
    sa.ForeignKeyConstraint(['deck_id'], ['decks.id'], ),
    sa.PrimaryKeyConstraint('deck_id', 'card_id')
    )
    op.create_table('gamedecks',
    sa.Column('game_id', sa.Integer(), nullable=False),
    sa.Column('deck_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['deck_id'], ['decks.id'], ),
    sa.ForeignKeyConstraint(['game_id'], ['games.id'], ),
    sa.PrimaryKeyConstraint('game_id', 'deck_id')
    )
    op.create_table('gameplayers',
    sa.Column('game_id', sa.Integer(), nullable=False),
    sa.Column('player_id', sa.Integer(), nullable=False),
    sa.Column('started_at', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['game_id'], ['games.id'], ),
    sa.ForeignKeyConstraint(['player_id'], ['players.id'], ),
    sa.PrimaryKeyConstraint('game_id', 'player_id')
    )
    op.create_table('handcards',
    sa.Column('hand_id', sa.Integer(), nullable=False),
    sa.Column('card_id', sa.Integer(), nullable=False),
    sa.Column('added_to_hand_at', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['card_id'], ['cards.id'], ),
    sa.ForeignKeyConstraint(['hand_id'], ['hands.id'], ),
    sa.PrimaryKeyConstraint('hand_id', 'card_id')
    )
    op.create_table('playershands',
    sa.Column('player_id', sa.Integer(), nullable=False),
    sa.Column('hand_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['hand_id'], ['hands.id'], ),
    sa.ForeignKeyConstraint(['player_id'], ['players.id'], ),
    sa.PrimaryKeyConstraint('player_id', 'hand_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('playershands')
    op.drop_table('handcards')
    op.drop_table('gameplayers')
    op.drop_table('gamedecks')
    op.drop_table('deckcards')
    op.drop_table('players')
    op.drop_table('hands')
    op.drop_table('games')
    op.drop_table('decks')
    op.drop_table('cards')
    # ### end Alembic commands ###
